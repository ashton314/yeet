use std::path::PathBuf;
use backend::StorageBackend;
use clap::{Parser, ValueEnum};
pub mod conf;
pub mod backend;

#[derive(Parser)]
#[command(version, about)]
struct Cli {
    /// Override default config file
    #[arg(short, long, value_name = "FILE")]
    config: Option<PathBuf>,

    /// Storage backend
    #[arg(short, long, value_enum, default_value_t = Backend::Aws)]
    backend: Backend,

    /// Slug (defaults to current timestamp)
    #[arg(short, long, value_name = "STR")]
    slug: Option<String>,

    /// File to yeet
    file: PathBuf
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
enum Backend {
    /// AWS S3 buckets
    Aws,
    /// Digital Ocean object storage
    Do
}

fn main() {
    let cli = Cli::parse();

    if let Some(c) = cli.config {
        eprintln!("Reading config from {:?}…", c.to_str());
    }

    let backend =
        match cli.backend {
            Backend::Aws => backend::aws::make(String::from("foo"), String::from("bar")),
            _ => panic!("Unimplemented backend")
        };

    println!("{}", backend.upload(cli.file));
}
