use std::path::PathBuf;

use super::StorageBackend;

pub struct Aws {
    bucket_name: String,
    object_key: String
}

pub fn make(bucket: String, obj_key: String) -> Aws {
    Aws {
        bucket_name: bucket,
        object_key: obj_key
    }
}

impl StorageBackend for Aws {
    fn upload(&self, path: PathBuf) -> String {
        let _file_content = std::fs::read(path).expect("Failed to read file content");
        format!("https://{}.s3.amazonaws.com/{}", self.bucket_name, self.object_key)
    }
}
