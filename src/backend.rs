use std::path::PathBuf;
pub mod aws;

pub trait StorageBackend {
    fn upload(&self, path: PathBuf) -> String;
}
